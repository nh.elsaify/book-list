import { combineReducers } from 'redux';
import booksReducer from './booksReducer';
import loadingReducer from './loadingReducer';
import categoriesReducer from './categoriesReducer';
import authorsReducer from './authorsReducer';
import modeReducer from './modeReducer';

const reducers = combineReducers({
  booksReducer,
  loadingReducer,
  categoriesReducer,
  authorsReducer,
  modeReducer
});

export default reducers;
