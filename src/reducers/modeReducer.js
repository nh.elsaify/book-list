import {
  TOGGLE_MODE,
  EXIT_TOGGLE_MODE,
} from '../actions/types';

const INIT_STATE = {
  editable: localStorage.getItem('editable')||false,
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {

    case TOGGLE_MODE:
      return { ...state, editable: action.payload };

    case EXIT_TOGGLE_MODE:
      return { ...state, editable: action.payload };

    default:
      return state;
  }
};
