import {
  ENABLE_LOADING,
  DISABLE_LOADING,
} from '../actions/types';

const INIT_STATE = {
  loading: false,
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case ENABLE_LOADING:
      return { ...state, loading: true };

    case DISABLE_LOADING:
      return { ...state, loading: false };

    default:
      return state;
  }
};
