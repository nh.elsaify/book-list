import {
  FETCH_CATEGORIES_SUCCESS,
} from '../actions/types';

const INIT_STATE = {
  categoriesList: null,
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case FETCH_CATEGORIES_SUCCESS:
      return { ...state, categoriesList: action.payload };

    default:
      return state;
  }
};
