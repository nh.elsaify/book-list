import {
  FETCH_BOOKS_SUCCESS,
  ADD_BOOK_FAILURE,
  EDIT_BOOK_FAILURE,
} from '../actions/types';

const INIT_STATE = {
  booksList: null,
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case FETCH_BOOKS_SUCCESS:
      return { ...state, booksList: action.payload };

    case ADD_BOOK_FAILURE:
      return{...state,error:action.payload}

    case EDIT_BOOK_FAILURE:
      return{...state,error:action.payload}

    default:
      return state;
  }
};
