import {
  FETCH_AUTHORS_SUCCESS,
} from '../actions/types';

const INIT_STATE = {
  authorsList: null,
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case FETCH_AUTHORS_SUCCESS:
      return { ...state, authorsList: action.payload };

    default:
      return state;
  }
};
