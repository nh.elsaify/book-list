import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './lib/appCss';
import Dashboard from './routes/dashboard/Dashboard';
import AddNewBook from './routes/books/AddNewBook';
import EditBook from './routes/books/EditBook';
import BookDetails from './routes/books/BookDetails';
import AddNewCategory from './routes/categories/AddNewCategory';
import EditCategory from './routes/categories/EditCategory';
import CategoryDetails from './routes/categories/CategoryDetails';
import AddNewAuthor from './routes/authors/AddNewAuthor';
import EditAuthor from './routes/authors/EditAuthor';
import AuthorDetails from './routes/authors/AuthorDetails';
import { configureStore } from './store';

const MainApp = () => (
	<Provider store={configureStore()}>
			<Router>
				<Switch>
					<Route path='/' component={Dashboard} exact={true}/>
          <Route path='/book/new' component={AddNewBook} exact={true}/>
          <Route path='/book/:bookId/edit' component={EditBook} exact={true} />
          <Route path='/book/:bookId' component={BookDetails} exact={true}/>
          <Route path='/category/new' component={AddNewCategory} exact={true}/>
          <Route path='/category/:categoryId/edit' component={EditCategory} exact={true}/>
          <Route path='/category/:categoryId' component={CategoryDetails} exact={true}/>
          <Route path='author/new' component={AddNewAuthor} exact={true}/>
          <Route path='/book/:authorId/edit' component={EditAuthor} exact={true}/>
          <Route path='/author/:authorId' component={AuthorDetails} exact={true}/>
				</Switch>
			</Router>
	</Provider>
);

export default MainApp;