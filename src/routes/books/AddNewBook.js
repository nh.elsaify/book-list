import React, { Component } from 'react';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import AppLayout from '../../containers/AppLayout';
import { fetchCategoriesList, addNewBook } from '../../actions';
import { Alert } from 'reactstrap';
import Form from 'react-jsonschema-form';
import uuidv1 from 'uuid/v1';

import {
  AddNewBookSchema, AddNewBookUiSchema,CustomFieldTemplate
} from './schema';

class AddNewBook extends Component {

  componentDidMount(){
    const { fetchCategoriesList } = this.props
    fetchCategoriesList()
  }
  handleCancelButton = (e) => {
  e.preventDefault();
  const { history } = this.props;
  history.push('/employees');
}

  handleAddButton = async (e) => {
    const { history, addNewBook } = this.props;
    let book ;
    book={
      author: e.formData.author,
      category: e.formData.category,
      description: e.formData.description,
      image: e.formData.image,
      isbn: e.formData.isbn,
      pagesNumber: e.formData.pagesNumber,
      publishYear: e.formData.publishYear,
      title: e.formData.title,
    }
    addNewBook({
      book,
    }, history);
  };

  render() {
    const {categoriesList, history} = this.props;

    return (
      <AppLayout history={history}>
        <div className='paper'>
          <p>Add New book</p>
          {!categoriesList&&(<Alert color='danger'>error</Alert>)}
          {categoriesList&&
            <Form
              schema={AddNewBookSchema(categoriesList)}
              onSubmit={(e)=>this.handleAddButton(e)}
              uiSchema={AddNewBookUiSchema}
              showErrorList={false}
              FieldTemplate={CustomFieldTemplate}
              noHtml5Validate
              >
                <div>
                  <Button variant="contained" color="primary" size="large" type="submit">Add Book</Button>
                  <Button variant="contained" color="default" size="large" onClick={this.handleCancelButton}>Cancel</Button>
                </div>
              </Form>
            }
        </div>
      </AppLayout>
    );
  }
}
const mapStateToProps = state => ({
  categoriesList: state.categoriesReducer.categoriesList
});

export default connect(mapStateToProps,
  {fetchCategoriesList, addNewBook})(AddNewBook);
