import React from 'react';
import {
  FormGroup,
  Label,
} from 'reactstrap';


const AddNewBookSchema = categories => ({
  type: 'object',
  required:['title', 'author', 'category', 'pagesNumber', 'isbn', 'publishYear', 'image', 'description'],
  properties: {
    title: {
      type: 'string',
      title: 'Title',
    },
    category: {
      type: 'string',
      title: 'Category',
      enum: categories.map(category => category.id),
      enumNames: categories.map(category => category.name),
    },
    author: {
      type: 'string',
      title: 'Author',
    },
    description: {
      type: 'string',
      title: 'Description',
    },
    isbn: {
      type: 'string',
      title: 'ISBN',
    },
    pagesNumber: {
      type: 'integer',
      title: 'No. of pages'
    },
    publishYear: {
      type: 'integer',
      title: 'Year published',
    },
    image: {
      "type": "string",
      "format": "uri"
    },
  }
})
const AddNewBookUiSchema = {
  description: {
    "ui:widget": "textarea",
    "ui:options": {
        "rows": 5
      },
  },
  image:{
   "ui:placeholder": "http://"
  }
  }

const CustomFieldTemplate = (props) => {
  const {
    label, help, description, errors, children, id,
  } = props;
  return (
    <FormGroup>
      <Label htmlFor={id}>{label}</Label>
      {description}
      {children}
      <p style={{ color: 'red' }}>{errors.props.errors}</p>
      {help}
    </FormGroup>
  );
}


export {
  AddNewBookSchema,
  AddNewBookUiSchema,
  CustomFieldTemplate,
};
