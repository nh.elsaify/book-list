import React, { Component } from 'react';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import AppLayout from '../../containers/AppLayout';
import { fetchCategoriesList, fetchBooks, fetchAuthors, editBook } from '../../actions';
import { Alert } from 'reactstrap';
import Form from 'react-jsonschema-form';

import {
  AddNewBookSchema, AddNewBookUiSchema, CustomFieldTemplate
} from './schema';

class EditBook extends Component {

  componentDidMount(){
    const { fetchCategoriesList, fetchBooks, fetchAuthors } = this.props
    fetchCategoriesList();
    fetchBooks();
    fetchAuthors();
  }

  componentDidUpdate(prevProps){
    const { editable,history } = this.props;
    if(prevProps.editable!==editable){
      !editable&&history.push('/')
    }
  }

  handleCancelButton = (e) => {
  e.preventDefault();
  const { history } = this.props;
  history.push('/');
  }

  handleAddButton = async (e) => {
    const { history, authorsList, editBook } = this.props;
    let book;
    let editablebook;
    let authorId;
    book = e.formData;
    authorId = authorsList.filter(author=>author.name===book.author)
    editablebook = {
      'author': authorId[0].id,
      'category': book.category,
      'description': book.description,
      'id': book.id,
      'image': book.image,
      'isbn': book.isbn,
      'pagesNumber': book.pagesNumber,
      'publishYear': book.publishYear,
      'title': book.title,
    }
    editBook({
      editablebook,
    }, history);
  };

  render() {
    const {categoriesList, booksList, authorsList, match, history} = this.props;
    let currentBook ;
    let authorName
    currentBook = booksList && booksList.filter(book=>book.id===match.params.bookId)
    authorName = (currentBook && authorsList) && authorsList.filter(author=>author.id===currentBook[0].author)
    currentBook = authorName && {
      'author': authorName[0].name,
      'category': currentBook[0].category,
      'description': currentBook[0].description,
      'id': currentBook[0].id,
      'image': currentBook[0].image,
      'isbn': currentBook[0].isbn,
      'pagesNumber': currentBook[0].pagesNumber,
      'publishYear': currentBook[0].publishYear,
      'title': currentBook[0].title,
    }
    return (
      <AppLayout history={history}>
        <div className='paper'>
          <p>Edit book {currentBook && `: ${currentBook.title}`}</p>
          {(!categoriesList||!booksList||!authorsList)&&(<Alert color='danger'>error</Alert>)}
          {categoriesList&&
            <Form
              schema={AddNewBookSchema(categoriesList)}
              onSubmit={(e)=>this.handleAddButton(e)}
              uiSchema={AddNewBookUiSchema}
              showErrorList={false}
              FieldTemplate={CustomFieldTemplate}
              noHtml5Validate
              formData={currentBook}
              >
                <div>
                  <Button variant="contained" color="primary" size="large" type="submit">Edit Book</Button>
                  <Button variant="contained" color="default" size="large" onClick={this.handleCancelButton}>Cancel</Button>
                </div>
              </Form>
            }
        </div>
      </AppLayout>
    );
  }
}
const mapStateToProps = state => ({
  categoriesList: state.categoriesReducer.categoriesList,
  booksList: state.booksReducer.booksList,
  authorsList: state.authorsReducer.authorsList,
  editable: state.modeReducer.editable,
});

export default connect(mapStateToProps,
  {fetchCategoriesList, fetchBooks, fetchAuthors, editBook})(EditBook);
