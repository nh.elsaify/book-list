import React, { Component } from 'react';
import { connect } from 'react-redux';
import AppLayout from '../../containers/AppLayout';
import BookDetailsCard from '../../components/bookDetailsCard';
import {fetchBooks} from '../../actions';
import { Alert } from 'reactstrap';

class BookDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    const { fetchBooks } = this.props;
    fetchBooks()
  }

  render() {
    const { booksList, history, match, editable } = this.props;
    const { bookId } = match.params;
    const book = booksList?booksList.filter(book=>book.id===bookId):null
    return (
    <AppLayout history={history}>
      {!book &&
        <Alert color="danger">
          something went wrong !
        </Alert>}
      {book&&<BookDetailsCard
        book={book}
        history={history}
        editable={editable}
      />}
    </AppLayout>
    );
  }
}
const mapStateToProps = state => ({
  booksList: state.booksReducer.booksList,
  editable: state.modeReducer.editable,
});

export default connect(mapStateToProps,
  {
    fetchBooks,
  })(BookDetails);
