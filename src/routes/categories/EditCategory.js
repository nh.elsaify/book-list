import React, { Component } from 'react';
import { connect } from 'react-redux';
import AppLayout from '../../containers/AppLayout';

class EditCategory extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
  }

  render() {
    const {history} = this.props;
    return (
      <AppLayout history={history}>
        <div className='paper'>
          <p>edit category</p>
        </div>
      </AppLayout>
    );
  }
}
const mapStateToProps = state => ({
  editable: state.modeReducer.editable,
});

export default connect(mapStateToProps,
  {})(EditCategory);
