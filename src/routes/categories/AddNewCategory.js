import React, { Component } from 'react';
import { connect } from 'react-redux';
import AppLayout from '../../containers/AppLayout';

class AddNewCategory extends Component {
  render() {
    const {history} = this.props;
    return (
      <AppLayout history={history}>
        <div className='paper'>
          <p>Add New category</p>
        </div>
      </AppLayout>
    );
  }
}
const mapStateToProps = state => ({
});

export default connect(mapStateToProps,
  {})(AddNewCategory);
