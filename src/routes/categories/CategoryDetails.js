import React, { Component } from 'react';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import AppLayout from '../../containers/AppLayout';
import BookCard from '../../components/bookcard';
import ReactPaginate from 'react-paginate';
import {fetchCategoriesList, fetchBooks} from '../../actions';
import { Alert } from 'reactstrap';
import Typography from '@material-ui/core/Typography';


class BookDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pageSize : 5,
      pagesCount : 0,
      currentPage: 0,
      search_terms:' ',
    }
  }

  componentDidMount() {
    const { fetchCategoriesList, fetchBooks } = this.props;
    fetchCategoriesList()
    fetchBooks()
  }

  handleClick(e, index) {
    this.setState({
      currentPage: e.selected+1
    });
  }



  render() {
    let category;
    let filteredBooks;
    let pagesCount;
    const { booksList, categoriesList, history, match, editable } = this.props;
    const { categoryId } = match.params;
    const { currentPage, pageSize } = this.state;
    category = categoriesList && categoriesList.filter(category=>category.id===categoryId)[0];
    filteredBooks = (booksList&&category) && booksList.filter(book=>book.category===category.id)
    pagesCount = filteredBooks&&(Math.ceil(filteredBooks.length / this.state.pageSize)-1)

    return (
    <AppLayout history={history}>
      {(!categoriesList||!booksList) &&
        <Alert color="danger">
          something went wrong !
        </Alert>
      }
      {category&&
        <div className='container category'>
          <div className='row'>
            <div className='col-sm-10'>
              <Typography gutterBottom variant="headline"> {category.name} </Typography>
            </div>
            <div className='col-sm-2'>
              {editable && (
                <Button variant="contained" color="primary" className="card-button" onClick={this.onEdit}>
                  edit
                </Button>
              )}
            </div>
          </div>
            {
              filteredBooks&&
              (
                <div className='col-sm-12 category-books'>
                {filteredBooks.length>0?filteredBooks.slice(
                       currentPage * pageSize,
                       (currentPage + 1) * pageSize
                     ).map(book =>{
                       return(
                           <BookCard
                             key={book.id}
                             editable={editable}
                             book={book}
                             history={history}
                           />
                       )
                     }): <Alert color="info">
                          This is no books to show !
                        </Alert>
                }
                <div className="paginationDiv">
                      <ReactPaginate
                        initialPage={currentPage}
                        previousLabel="&laquo;"
                        nextLabel="&raquo;"
                        breakLabel={<a href="">...</a>}
                        breakClassName="active"
                        pageCount={pagesCount}
                        marginPagesDisplayed={2}
                        pageRangeDisplayed={2}
                        onPageChange={e => this.handleClick(e,currentPage)}
                        containerClassName="pagination"
                        subContainerClassName="paginationSub"
                        activeClassName="active"
                      />
                </div>
              </div>)
            }
        </div>
      }
    </AppLayout>
    );
  }
}
const mapStateToProps = state => ({
  booksList: state.booksReducer.booksList,
  categoriesList: state.categoriesReducer.categoriesList,
  editable: state.modeReducer.editable,
});

export default connect(mapStateToProps,
  {
    fetchCategoriesList,
    fetchBooks,
  })(BookDetails);
