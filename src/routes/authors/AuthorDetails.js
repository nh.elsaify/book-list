import React, { Component } from 'react';
import { connect } from 'react-redux';
import AppLayout from '../../containers/AppLayout';

class AuthorDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
  }

  render() {
    const {history} = this.props;
    return (
      <AppLayout history={history}>
        <div className='paper'>
          <p>Author details</p>
        </div>
      </AppLayout>
    );
  }
}
const mapStateToProps = state => ({
});

export default connect(mapStateToProps,
  {
  })(AuthorDetails);
