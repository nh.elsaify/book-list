import React, { Component } from 'react';
import { connect } from 'react-redux';
import AppLayout from '../../containers/AppLayout';

class AddNewAuthor extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
  }

  render() {
    const {history} = this.props;
    return (
      <AppLayout history={history}>
        <div className='paper'>
          <p>Add New Author</p>
        </div>
      </AppLayout>
    );
  }
}
const mapStateToProps = state => ({
});

export default connect(mapStateToProps,
  {
  })(AddNewAuthor);
