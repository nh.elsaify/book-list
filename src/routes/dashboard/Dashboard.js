import React, { Component } from 'react';
import { connect } from 'react-redux';
import AppLayout from '../../containers/AppLayout';
import BookCard from '../../components/bookcard';
import Input from '@material-ui/core/Input';
import ReactPaginate from 'react-paginate';
import SearchIcon from '@material-ui/icons/Search';
import { Alert } from 'reactstrap';
import {fetchBooks} from '../../actions';
import _ from 'lodash';

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pageSize : 5,
      pagesCount : 0,
      currentPage: 0,
      search_terms:' ',
    }
}

  componentDidMount() {
    const { fetchBooks } = this.props;
    fetchBooks();
  }

  handleClick(e, index) {
    this.setState({
      currentPage: e.selected+1
    });
  }

  componentDidUpdate(prevProps,prevState){
    const { booksList } = this.props;
    if(prevProps.booksList!==booksList){
      this.setState({
        pagesCount : Math.ceil(booksList.length / this.state.pageSize)-1
      })
    }
  }

  onSearch = (e) => {
    // const { booksList } = this.props;
    this.setState({
      search_terms : e.target.value.length>0?e.target.value:' ',
    })

  }

  render() {
    const { booksList, history,editable } = this.props;
    const { currentPage, pagesCount, pageSize, search_terms } = this.state;
    let filteredBooks;
    if(booksList){
      filteredBooks = booksList.filter(
        book => _.includes(`${book.title} `.toUpperCase(),search_terms.toUpperCase())
      )
    }
    return (
    <AppLayout history={history}>
      {(filteredBooks&&!filteredBooks.length)>0 &&
        (<Alert color="info">
             This is no books to show !
           </Alert>)
      }
      {(filteredBooks&&filteredBooks.length>0)&&(
        <div>
          <div className='search'>
                <div className='searchIcon'>
                  <SearchIcon />
                </div>
                <Input
                  placeholder="Search…"
                  disableUnderline
                  onChange={e=>this.onSearch(e)}
                />
          </div>
            <div>
            {filteredBooks.length>0&&filteredBooks.slice(
                   currentPage * pageSize,
                   (currentPage + 1) * pageSize
                 ).map(book =>{
                   return(
                       <BookCard
                         key={book.id}
                         editable={editable}
                         book={book}
                         history={history}
                       />
                   )
                 })
            }
            <div className="paginationDiv">
                  <ReactPaginate
                    initialPage={currentPage}
                    previousLabel="&laquo;"
                    nextLabel="&raquo;"
                    breakLabel={<a href="">...</a>}
                    breakClassName="active"
                    pageCount={pagesCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={2}
                    onPageChange={e => this.handleClick(e,currentPage)}
                    containerClassName="pagination"
                    subContainerClassName="paginationSub"
                    activeClassName="active"
                  />
            </div>
          </div>
        </div>
      )
      }
    </AppLayout>
    );
  }
}
const mapStateToProps = state => ({
  booksList: state.booksReducer.booksList,
  editable: state.modeReducer.editable,
});

export default connect(mapStateToProps,
  {fetchBooks})(Dashboard);
