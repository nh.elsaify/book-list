import {
  TOGGLE_MODE,
  EXIT_TOGGLE_MODE,
} from './types';


export const editMode = () => dispatch => {
  localStorage.setItem('editable',true);
  dispatch({
    type: TOGGLE_MODE, payload: true
  })
}

export const exitEditMode = () => dispatch => {
  localStorage.removeItem('editable');
  dispatch({
    type: EXIT_TOGGLE_MODE, payload: false
  })
}
