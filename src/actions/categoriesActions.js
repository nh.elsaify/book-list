import {
  ENABLE_LOADING,
  DISABLE_LOADING,
  FETCH_CATEGORIES_SUCCESS,
} from './types';
import axios from 'axios';

export const fetchCategoriesList = () => (dispatch) => {
  dispatch({ type: ENABLE_LOADING });
  try{
    axios.get('http://localhost:3001/categories')
    .then(function (response) {
      dispatch({ type: FETCH_CATEGORIES_SUCCESS,payload:response.data });
    })
  } catch (e){} finally{
    dispatch({ type: DISABLE_LOADING });
  }
}
