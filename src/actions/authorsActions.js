import {
  ENABLE_LOADING,
  DISABLE_LOADING,
  FETCH_AUTHORS_SUCCESS,
} from './types';
import axios from 'axios';

export const fetchAuthors = () => (dispatch) => {
  dispatch({ type: ENABLE_LOADING });
  try{
    axios.get('http://localhost:3001/authors')
    .then(function (response) {
      dispatch({ type: FETCH_AUTHORS_SUCCESS,payload:response.data });
    })
  } catch (e){} finally{
    dispatch({ type: DISABLE_LOADING });
  }
}
