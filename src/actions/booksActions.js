import {
  ENABLE_LOADING,
  DISABLE_LOADING,
  FETCH_BOOKS_SUCCESS,
  ADD_BOOK_FAILURE,
  EDIT_BOOK_FAILURE
} from './types';
import axios from 'axios';

export const fetchBooks = () => (dispatch) => {
  dispatch({ type: ENABLE_LOADING });
  try{
    axios.get('http://localhost:3001/books')
    .then(function (response) {
      dispatch({ type: FETCH_BOOKS_SUCCESS,payload:response.data });
    })
  } catch (e){} finally{
    dispatch({ type: DISABLE_LOADING });
  }
}

export const addNewBook = (book,history) => (dispatch) => {
  dispatch({ type: ENABLE_LOADING });
  try{
    axios.post('http://localhost:3001/books',book.book)
    .then(function (response) {
      history.push('/')
    })
  } catch (e){
    dispatch({ type: ADD_BOOK_FAILURE,payload:e });
  } finally{
    dispatch({ type: DISABLE_LOADING });
  }
}

export const editBook = (book,history) => (dispatch) => {
  dispatch({ type: ENABLE_LOADING });
  try{
    axios.patch(`http://localhost:3001/books/${book.editablebook.id}`,book.editablebook)
    .then(function (response) {
      history.push('/')
    })
  } catch (e){
    dispatch({ type: EDIT_BOOK_FAILURE,payload:e });
  } finally{
    dispatch({ type: DISABLE_LOADING });
  }
}
