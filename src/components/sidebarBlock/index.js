import React, { Component } from 'react';
import { connect } from 'react-redux';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ChromeReaderMode from '@material-ui/icons/ChromeReaderMode';
import Add from '@material-ui/icons/Add';
import Author from '@material-ui/icons/People';
import Apps from '@material-ui/icons/Apps';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Collapse from '@material-ui/core/Collapse';
import List from '@material-ui/core/List';
import { fetchCategoriesList, fetchBooks, fetchAuthors, editBook } from '../../actions';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

const ITEM_HEIGHT = 48;

const CustomList = (props) => {
  const { onHeaderClick, open, headerText, children } = props;
  return (
    <List>
      <ListItem button onClick={onHeaderClick}>
        <ListItemText primary={headerText} />
        {open ? <ExpandLess /> : <ExpandMore />}
      </ListItem>
      <Collapse in={open} timeout="auto" unmountOnExit>
        {children}
      </Collapse>
    </List>
  )
}

class SidebarList extends Component {
  state = {
    open: false,
    anchorEl: null,
  };


  handleClick2 = event => {
   this.setState({ anchorEl: event.currentTarget });
 };

 handleClose = () => {
   this.setState({ anchorEl: null });
 };


  componentDidMount(){
    const { fetchCategoriesList, fetchBooks, fetchAuthors } = this.props
    fetchCategoriesList();
    fetchBooks();
    fetchAuthors();
  }
  handleClick = (list) => {
    this.setState(prevState => ({
      ...prevState,
      open: {
        ...prevState.open,
        [list]: !prevState.open[list]
      }
    }))
  };
  openPage = (path) => {
    const {history} = this.props;
    history.push(path)
  }

  render() {
    const { categoriesList, authorsList } = this.props;
    const { anchorEl } = this.state;

    return (
      <div className='sidebarContainer'>
        <CustomList
        headerText='Books'
        onHeaderClick={() => this.handleClick('booksList')}
        open={this.state.open.booksList}
      >
        <List>
          <ListItem button onClick={()=>this.openPage('/')}>
            <ListItemIcon><ChromeReaderMode /></ListItemIcon>
            <ListItemText inset primary="Books List" />
          </ListItem>
          <ListItem button onClick={()=>this.openPage('/book/new')}>
            <ListItemIcon><Add /></ListItemIcon>
            <ListItemText inset primary="Add New Book" />
          </ListItem>
        </List>
      </CustomList>
      <CustomList
        headerText='Authors'
        onHeaderClick={() => this.handleClick('authorsList')}
        open={this.state.open.authorsList}
      >
        <List>
          <ListItem button onClick={()=>this.openPage('/author/new')}>
            <ListItemIcon><Add /></ListItemIcon>
            <ListItemText inset primary="Add New Author" />
          </ListItem>
          <ListItem button aria-label="More" aria-owns={anchorEl ? 'long-menu' : null} aria-haspopup="true" onClick={this.handleClick2}>
            <ListItemIcon><Author /></ListItemIcon>
            <ListItemText inset primary="Authors List" />
          </ListItem>
          <Menu id="long-menu" anchorEl={this.state.anchorEl} open={Boolean(anchorEl)}
            onClose={this.handleClose}
            PaperProps={{
              style: {
                maxHeight: ITEM_HEIGHT * 4.5,
                width: 200,
              },
            }}>
            {authorsList &&
              authorsList.map(
                author=>(
                  <MenuItem key={author.id} onClick={()=>this.openPage(`/author/${author.id}`)}>{author.name}</MenuItem>
                )
              )
            }
          </Menu>
        </List>
      </CustomList>
      <CustomList
        headerText='Categories'
        onHeaderClick={() => this.handleClick('categories')}
        open={this.state.open.categories}
      >
        <List>
          <ListItem button onClick={()=>this.openPage('/category/new')}>
            <ListItemIcon><Add /></ListItemIcon>
            <ListItemText inset primary="Add New Category" />
          </ListItem>
          <ListItem button aria-label="More" aria-owns={anchorEl ? 'long-menu' : null} aria-haspopup="true" onClick={this.handleClick2}>
            <ListItemIcon><Apps /></ListItemIcon>
            <ListItemText inset primary="Categories List" />
          </ListItem>
          <Menu id="long-menu" anchorEl={this.state.anchorEl} open={Boolean(anchorEl)}
            onClose={this.handleClose}
            PaperProps={{
              style: {
                maxHeight: ITEM_HEIGHT * 4.5,
                width: 200,
              },
            }}>
            {categoriesList &&
              categoriesList.map(
                category=>(
                  <MenuItem key={category.id} onClick={()=>this.openPage(`/category/${category.id}`)}>{category.name}</MenuItem>
              )
            )
          }
          </Menu>
        </List>
      </CustomList>
      </div>
    )
  }
}


const mapStateToProps = state => ({
  categoriesList: state.categoriesReducer.categoriesList,
  booksList: state.booksReducer.booksList,
  authorsList: state.authorsReducer.authorsList,
});

export default connect(mapStateToProps,
  {fetchCategoriesList, fetchBooks, fetchAuthors, editBook})(SidebarList);
