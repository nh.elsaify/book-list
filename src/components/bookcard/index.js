import React,{Component} from 'react';
import Button from '@material-ui/core/Button';
import {Card, CardText, CardImg } from 'reactstrap';
import Grid from '@material-ui/core/Grid';
import _ from 'lodash';
import Typography from '@material-ui/core/Typography';

class BookCard extends Component{
  onEdit = () => {
    const {history,book} = this.props;
    history.push(`/book/${book.id}/edit`)
  }
  openBookDetails = () => {
    const {history,book} = this.props;
    history.push(`/book/${book.id}`)
  }
    render(){
      const { editable, book } = this.props;
      const { image, title, description } = book;
      return (
        <Card className='bookcard'>
          <Grid container spacing={24}>
            <Grid item xs={4} className="card-image">
              <CardImg width='100%' src={image} alt={title} />
            </Grid>
            <Grid container item xs={8}>
              <Grid container item className='cardheader' xs={12}>
                <Grid item xs={12}>
                  <Typography gutterBottom variant="title"> {title} </Typography>
                </Grid>
              </Grid>
              <Grid item xs={12}>
                <CardText>{
                  _.truncate(description, {
                    'length': 200,
                    'separator': '.'
                  })}
                </CardText>
              </Grid>
              <Grid container item xs={12} className='cardfooter'>
                <Grid item xs={12}>
                  {editable && (
                    <Button variant="contained" color="primary" className="card-button" onClick={this.onEdit}>
                      edit
                    </Button>
                  )}
                  <Button variant="contained" color="primary" className={"card-button details"} onClick={this.openBookDetails}>
                    details
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Card>
      )

    }
}
export default BookCard;
