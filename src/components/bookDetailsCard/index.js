import React,{Component} from 'react';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import {Card, CardImg } from 'reactstrap';

const CustomTypography = (props) => {
  const { label, text} = props;
  return(
    <p className='typo-book-details'>
      <span className='typo-book-details-label'>{label} </span>
      {text}
    </p>
  )
}

class BookDetailsCard extends Component{
  onEdit = () => {
    const {history,book} = this.props;
    const bookID = book[0].id
    history.push(`/book/${bookID}/edit`)
  }
    render(){
      const { book, editable } = this.props;
      const { image, title, description, author, pagesNumber, publishYear, isbn, category } = book[0]

      return (
          <Card className='bookcard'>
              <Grid container spacing={24}>
                <Grid item xs={8}>
                  <Typography gutterBottom variant="headline"> {title} </Typography>
                  <div className='details'>
                    <CustomTypography label='By :' text={author}/>
                    <CustomTypography label='Number of pages:' text={pagesNumber} />
                    <CustomTypography label='Publish year: ' text={publishYear} />
                    <CustomTypography label='ISBN: ' text={isbn} />
                    <CustomTypography label='Classification: ' text={category} />
                  </div>
                </Grid>
                <Grid container item xs={4} className="card-image">
                  <Grid item xs={12}>
                    {editable && (
                      <Button variant="contained" color="primary" className="card-button" onClick={this.onEdit}>
                        edit
                      </Button>
                    )}
                  </Grid>
                  <Grid item xs={12}>
                    <CardImg width='100%' src={image} alt={title} />
                  </Grid>
                </Grid>
                <Grid container item xs={12}>
                  <Typography gutterBottom variant="subheading">
                    {description}
                  </Typography>
                </Grid>
              </Grid>
            </Card>
      )

    }
}
const mapStateToProps = state => ({
  editable: state.modeReducer.editable,
});

export default connect(mapStateToProps,
  {})(BookDetailsCard);
