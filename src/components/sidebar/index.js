import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Drawer from '@material-ui/core/Drawer';
import { withStyles } from '@material-ui/core/styles';
import SidebarList  from '../sidebarBlock';
import { Hidden } from '@material-ui/core';
const drawerWidth = 240;

const styles = theme => ({
    navIconHide: {
        [theme.breakpoints.up('md')]: {
            display: 'none',
        },
    },
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
        width: drawerWidth,
        height:'100%',
        position:'absolute'
    },
});

class Sidebar extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        const { classes, history } = this.props;
        const drawer = (
            <div>
                <div className={classes.toolbar} />
                <SidebarList
                  history={history}
                />
            </div>
        );
        return (
            <div className='sidebar-div'>

                <Hidden mdUp>
                    <Drawer
                        variant="temporary"
                        open={this.props.mobileOpen}
                        onClose={this.props.handleDrawerToggle}
                        classes={{
                            paper: classes.drawerPaper,
                        }}
                        ModalProps={{
                            keepMounted: true,
                        }}
                    >
                        {drawer}
                    </Drawer>
                </Hidden>
                <Hidden smDown implementation="css">
                    <Drawer
                        variant="permanent"
                        open
                        classes={{paper: classes.drawerPaper}}
                    >
                        {drawer}
                    </Drawer>
                </Hidden>
            </div>
        );
    }
}

Sidebar.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(Sidebar);
