import React, { Component } from 'react';
import { connect } from 'react-redux';
import IconButton from '@material-ui/core/IconButton';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import MenuIcon from '@material-ui/icons/Menu';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import {editMode, exitEditMode} from '../../actions';

class Header extends Component {
	state = {
		editable: localStorage.getItem('editable')
	}
	editMode = () => {
		this.props.editMode()
	}
	exitEditMode = () => {
		this.props.exitEditMode()
	}

	render() {
		const { editable } = this.props;
		return (
			<AppBar
				position="absolute"
				className={editable?'appBar editable':'appBar'}
			>
				<Toolbar disableGutters className='toolbar'>
					<IconButton
						color="inherit"
						aria-label="Open drawer"
						onClick={this.props.handleDrawerToggle}
						className={this.props.className}
					>
						<MenuIcon />
					</IconButton>
					<Typography variant="title" color="inherit" noWrap className='title'>
						Booking List
						{editable&&<span className='editmode-span'>edit mode</span>}
					</Typography>
					{editable?
						<Button color="inherit" onClick={()=>this.exitEditMode()}>exit edit mode</Button>
						:<Button color="inherit" onClick={()=>this.editMode()}>edit mode</Button>

					}
				</Toolbar>
			</AppBar>
		);
	}
}

const mapStateToProps = state => ({
  editable: state.modeReducer.editable,
});

export default connect(mapStateToProps,
  {editMode, exitEditMode})(Header);
